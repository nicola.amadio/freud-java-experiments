/* Usage: java MainDriver [soot-options] appClass
*/
/* import necessary soot packages */
import soot.*;
public class MainDriver {

    public static void main(String[] args) {
        /* check the arguments */
        if (args.length == 0) {
           System.err.println("Usage: java MainDriver [options] classname");
           System.exit(0);
        }

        // add the path to your rt.jar for your machine
        Scene.v().setSootClassPath("/usr/lib/jvm/jdk1.8.0_261/jre/lib/rt.jar"
                + ":" + System.getProperty("user.dir") + "/target/classes");

        /* add a phase to transformer pack by call Pack.add */
        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.instrumenter",
                new InvokeStaticInstrumenter()));
        /* Give control to Soot to process all options,
        * InvokeStaticInstrumenter.internalTransform will get called.
        */
        soot.Main.main(args);
        }
    }